/**
 * @author    Agence Dn'D <contact@dnd.fr>
 * @copyright 2004-present Agence Dn'D
 * @license   https://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      https://www.dnd.fr/
 */

define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('dnd.styleguideMenu', {
        _create: function () {
            this.menu = this.element; // jQuery object
            this.menuItems = this.menu.find('li > a'); // jQuery object
            this.sectionOffset = '30';

            this._addSmoothScroll();
            this._menuItemsHandler();
            this._activateClassOnScroll();
        },

        /**
         * Add smooth scroll
         * @private
         */
        _addSmoothScroll: function() {
            $('html').css({
                'scroll-behavior': 'smooth',
                'scroll-padding-top': `${this.sectionOffset}px`
            });
        },

        /**
         * Menu items handler
         * @private
         */
        _menuItemsHandler: function() {
            const self = this;

            this.menuItems.on('click', function(event) {
                self.menuItems.removeAttr('class');
                $(this).addClass('active');
            });
        },

        /**
         * Activate class on scroll
         * @private
         */
        _activateClassOnScroll: function() {
            const self = this;

            $(window).scroll(function() {
                const scrollDistance = $(window).scrollTop();

                $('.styleguide__section').each(function(i) {
                    const currentSectionId = $(this).attr('id');
                    const scrollDisantceOffset = parseInt(scrollDistance) + parseInt(self.sectionOffset);

                    if ($(this).position().top <= scrollDisantceOffset) {
                        self.menuItems.removeAttr('class');
                        self.menu
                                .find(`li > a[href="#${currentSectionId}"]`)
                                .addClass('active');
                    }
                });
            }).scroll();
        }
    });

    return $.dnd.styleguideMenu;
});
